package net.riversoft.worktimetransference.service

import net.riversoft.worktimetransference.model.employee.Employee
import net.riversoft.worktimetransference.model.event.Event
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.DuplicateKeyException
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

import java.time.LocalDate
import java.time.LocalDateTime

@RunWith(SpringRunner)
@SpringBootTest
@ActiveProfiles("test")
class UserEventServiceTest {

    @Autowired
    private UserEventService userEventService

    @Test(expected = DuplicateKeyException)
    void checkPossibilityAddingIdenticalEvents_test() {
        def unique = new Event(
                new Employee("korolev_p", "Королев Павел"),
                new Employee("korolev_p", "Королев Павел"),
                LocalDateTime.of(2017, 10, 9, 10, 0),
                LocalDateTime.of(2017, 10, 9, 11, 0),
                LocalDate.of(2017, 10, 10))
        // given
        userEventService.addNewEvent(unique)
        def alreadyExistEvent =
                new Event(
                        new Employee("korolev_p", "Королев Павел"),
                        new Employee("korolev_p", "Королев Павел"),
                        LocalDateTime.of(2017, 10, 9, 10, 0),
                        LocalDateTime.of(2017, 10, 9, 11, 0),
                        LocalDate.of(2017, 10, 10))
        // then try to add
        userEventService.addNewEvent(alreadyExistEvent)
    }

}
