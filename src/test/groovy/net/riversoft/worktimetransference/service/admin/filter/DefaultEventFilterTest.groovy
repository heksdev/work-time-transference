package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.model.employee.Employee
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

import java.time.LocalDate
import java.time.LocalDateTime

import static org.assertj.core.api.Java6Assertions.assertThat

@Slf4j("logger")
@RunWith(SpringRunner)
@SpringBootTest
@ActiveProfiles("test")
class DefaultEventFilterTest extends EventFilterTestTemplate {

    @Test
    void findAllEvents_test() {
        EventFilter eventFilter = new DefaultEventFilter(repository)
        // given
        EventFilterDTO dto = new EventFilterDTO() // dto can be empty
        List<Event> expected = Arrays.asList(
                new Event(
                        new Employee("hal_a", "Галь Александр"),
                        new Employee("hal_a", "Галь Александр"),
                        LocalDateTime.of(2017, 10, 11, 9, 0),
                        LocalDateTime.of(2017, 10, 11, 10, 0),
                        LocalDate.of(2017, 10, 11)),
                new Event(
                        new Employee("hal_a", "Галь Александр"),
                        new Employee("hal_a", "Галь Александр"),
                        LocalDateTime.of(2017, 10, 10, 9, 0),
                        LocalDateTime.of(2017, 10, 10, 10, 0),
                        LocalDate.of(2017, 10, 10)),
                new Event(
                        new Employee("hal_a", "Галь Александр"),
                        new Employee("hal_a", "Галь Александр"),
                        LocalDateTime.of(2017, 10, 1, 9, 0),
                        LocalDateTime.of(2017, 10, 10, 10, 0),
                        LocalDate.of(2017, 10, 10)),
                new Event(
                        new Employee("korolev_p", "Королев Павел"),
                        new Employee("korolev_p", "Королев Павел"),
                        LocalDateTime.of(2017, 10, 9, 10, 0),
                        LocalDateTime.of(2017, 10, 9, 11, 0),
                        LocalDate.of(2017, 10, 10)),
                new Event(
                        new Employee("levchenko_r", "Левченко Роман"),
                        new Employee("levchenko_r", "Левченко Роман"),
                        LocalDateTime.of(2017, 10, 10, 10, 0),
                        LocalDateTime.of(2017, 10, 11, 10, 0),
                        LocalDate.of(2017, 10, 11)),
                new Event(
                        new Employee("serdyuk_e", "Serdyuk Elen"),
                        new Employee("serdyuk_e", "Serdyuk Elen"),
                        LocalDateTime.of(2017, 10, 10, 13, 0),
                        LocalDateTime.of(2017, 10, 10, 14, 0),
                        LocalDate.of(2017, 10, 11)),
                new Event(
                        new Employee("gladkiy_v", "Гладкий Виталий"),
                        new Employee("gladkiy_v", "Гладкий Виталий"),
                        LocalDateTime.of(2017, 10, 12, 9, 0),
                        LocalDateTime.of(2017, 10, 13, 9, 0),
                        LocalDate.of(2017, 10, 14))
        )
        // when
        List<Event> result = eventFilter.find(dto)
        // then
        assertThat(expected.size() == result.size() && result.containsAll(expected))
    }

    @Test
    void findAllEventsWithNullDTO_test() {
        EventFilter eventFilter = new DefaultEventFilter(repository)
        // given
        EventFilterDTO dto = null
        // just call filter
        eventFilter.find(dto)
    }
}
