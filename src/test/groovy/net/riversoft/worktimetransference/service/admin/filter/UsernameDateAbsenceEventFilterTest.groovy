package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.exception.DtoBindException
import net.riversoft.worktimetransference.model.employee.Employee
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

import java.time.LocalDate
import java.time.LocalDateTime

import static org.assertj.core.api.Java6Assertions.assertThat
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy

@Slf4j("logger")
@RunWith(SpringRunner)
@SpringBootTest
@ActiveProfiles("test")
class UsernameDateAbsenceEventFilterTest extends EventFilterTestTemplate {

    @Test
    void findByUsernameAndBetweenTwoDates() {
        EventFilter eventFilter = new UsernameDateAbsenceEventFilter(repository)
        // given
        EventFilterDTO dto = new EventFilterDTO()
        dto.setEmployeeUsername("hal_a")
        dto.setDateFrom(LocalDate.of(2017, 10, 11))
        dto.setAnotherDate(LocalDate.of(2017, 10, 12))
        List<Event> expected = [new Event(
                new Employee("hal_a", "Галь Александр"),
                new Employee("hal_a", "Галь Александр"),
                LocalDateTime.of(2017, 10, 11, 9, 0),
                LocalDateTime.of(2017, 10, 11, 10, 0),
                LocalDate.of(2017, 10, 11))]
        // when
        List<Event> result = eventFilter.find(dto)
        // then
        assertThat(expected.size() == result.size() && result.containsAll(expected))
    }

    @Test
    void findByUsernameDateAbsenceWithoutSpecifiedUsernameInDto_test() {
        // given
        EventFilter eventFilter = new UsernameDateAbsenceEventFilter(repository)
        def dto = new EventFilterDTO()
        String expectedMessage = "Обязательные поля для фильтра \"" +
                UsernameDateAbsenceEventFilter.CRITERIA.description +
                "\" - [" + EventFilterDTO.USERNAME_VIEW_NAME + ", " + EventFilterDTO.DATE_FROM_VIEW_NAME + "]"
        // when
        assertThatThrownBy{eventFilter.find(dto)}.isInstanceOf(DtoBindException.class)
        assertThatThrownBy{eventFilter.find(dto)}.hasMessage(expectedMessage)
    }
}
