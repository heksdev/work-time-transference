package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.exception.DtoBindException
import net.riversoft.worktimetransference.model.employee.Employee
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

import java.time.LocalDate
import java.time.LocalDateTime

import static org.assertj.core.api.Java6Assertions.assertThat
import static org.assertj.core.api.Java6Assertions.assertThatThrownBy

@Slf4j("logger")
@RunWith(SpringRunner)
@SpringBootTest
@ActiveProfiles("test")
class DateAbsenceEventFilterTest extends EventFilterTestTemplate {

    @Test
    void findByDateFrom_test() {
        EventFilter eventFilter = new DateAbsenceEventFilter(repository)
        // given
        EventFilterDTO dto = new EventFilterDTO()
        dto.setDateFrom(LocalDate.of(2017, 10, 12))
        List<Event> expected = [new Event(
                new Employee("gladkiy_v", "Гладкий Виталий"),
                new Employee("gladkiy_v", "Гладкий Виталий"),
                LocalDateTime.of(2017, 10, 12, 9, 0),
                LocalDateTime.of(2017, 10, 13, 9, 0),
                LocalDate.of(2017, 10, 14))]
        // when
        List<Event> result = eventFilter.find(dto)
        // then
        assertThat(expected.size() == result.size() && result.containsAll(expected))
    }

    @Test
    void findByDateOfAbsenceWithoutSpecifiedDateInDTO_test() {
        // given
        def filter = new DateAbsenceEventFilter(repository)
        EventFilterDTO dto = new EventFilterDTO() // all fields is null
        String expectedMessage = "Обязательные поля для фильтра \"" +
                DateAbsenceEventFilter.CRITERIA.description +
                "\" - [" + EventFilterDTO.DATE_FROM_VIEW_NAME + "]"
        // when
        assertThatThrownBy{filter.find(dto)}.isInstanceOf(DtoBindException.class)
        assertThatThrownBy{filter.find(dto)}.hasMessage(expectedMessage)
    }
}