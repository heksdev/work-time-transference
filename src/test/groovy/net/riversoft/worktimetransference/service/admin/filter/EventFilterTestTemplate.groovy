package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.model.employee.Employee
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.repository.EventRepository
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ActiveProfiles

import java.time.LocalDate
import java.time.LocalDateTime

@ActiveProfiles("test")
@Slf4j("logger")
abstract class EventFilterTestTemplate {

    @Autowired
    protected EventRepository repository

    @Before
    void setup() {
        logger.debug(">>>>>>>>>>>>>>>>>>>>>>clear data")
        repository.deleteAll()
        logger.debug(">>>>>>>>>>>>>>>>>>>>>>insert testdata data")
        List<Event> eventList = Arrays.asList(
                new Event(
                        new Employee("hal_a", "Галь Александр"),
                        new Employee("hal_a", "Галь Александр"),
                        LocalDateTime.of(2017, 10, 11, 9, 0),
                        LocalDateTime.of(2017, 10, 11, 10, 0),
                        LocalDate.of(2017, 10, 11)),
                new Event(
                        new Employee("hal_a", "Галь Александр"),
                        new Employee("hal_a", "Галь Александр"),
                        LocalDateTime.of(2017, 10, 10, 9, 0),
                        LocalDateTime.of(2017, 10, 10, 10, 0),
                        LocalDate.of(2017, 10, 10)),
                new Event(
                        new Employee("hal_a", "Галь Александр"),
                        new Employee("hal_a", "Галь Александр"),
                        LocalDateTime.of(2017, 10, 1, 9, 0),
                        LocalDateTime.of(2017, 10, 10, 10, 0),
                        LocalDate.of(2017, 10, 10)),
                new Event(
                        new Employee("korolev_p", "Королев Павел"),
                        new Employee("korolev_p", "Королев Павел"),
                        LocalDateTime.of(2017, 10, 9, 10, 0),
                        LocalDateTime.of(2017, 10, 9, 11, 0),
                        LocalDate.of(2017, 10, 10)),
                new Event(
                        new Employee("levchenko_r", "Левченко Роман"),
                        new Employee("levchenko_r", "Левченко Роман"),
                        LocalDateTime.of(2017, 10, 10, 10, 0),
                        LocalDateTime.of(2017, 10, 11, 10, 0),
                        LocalDate.of(2017, 10, 11)),
                new Event(
                        new Employee("serdyuk_e", "Serdyuk Elen"),
                        new Employee("serdyuk_e", "Serdyuk Elen"),
                        LocalDateTime.of(2017, 10, 10, 13, 0),
                        LocalDateTime.of(2017, 10, 10, 14, 0),
                        LocalDate.of(2017, 10, 11)),
                new Event(
                        new Employee("gladkiy_v", "Гладкий Виталий"),
                        new Employee("gladkiy_v", "Гладкий Виталий"),
                        LocalDateTime.of(2017, 10, 12, 9, 0),
                        LocalDateTime.of(2017, 10, 13, 9, 0),
                        LocalDate.of(2017, 10, 14))
        )
        eventList.each {
            event -> repository.insert(event)
        }
    }
}
