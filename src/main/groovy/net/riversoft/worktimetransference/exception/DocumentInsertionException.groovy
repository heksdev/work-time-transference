package net.riversoft.worktimetransference.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.CONFLICT, reason = 'Ошибка при попытке добавить документ')
class DocumentInsertionException extends RuntimeException {

    DocumentInsertionException(String message) {
        super(message)
    }
}
