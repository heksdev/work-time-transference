package net.riversoft.worktimetransference.exception

import net.riversoft.worktimetransference.service.admin.filter.EventSearchCriteria
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Не правильно заполнена форма")
class DtoBindException extends RuntimeException {

    private static final String MESSAGE_TEMPLATE = "Обязательные поля для фильтра \"%s\" - %s"

    DtoBindException(EventSearchCriteria criteria, String...fields) {
        super(String.format(MESSAGE_TEMPLATE, criteria.description, fields.toArrayString()))
    }
}
