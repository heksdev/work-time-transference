package net.riversoft.worktimetransference.controller

import groovy.util.logging.Slf4j
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.RequestMapping

import javax.servlet.http.HttpServletRequest

@Slf4j
@Controller
class ExceptionHandleController {

    @RequestMapping("/access-denied")
    String accessDenied(HttpServletRequest request, ModelMap model) {
        String comeInUrl = request.getHeader("referer")
        log.debug(comeInUrl)
        if (comeInUrl == null) {
            comeInUrl = "/login"
        }
        model.addAttribute("comeInUrl", comeInUrl)
        "access-denied"
    }
}
