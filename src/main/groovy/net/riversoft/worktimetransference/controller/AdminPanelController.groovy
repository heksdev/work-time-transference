package net.riversoft.worktimetransference.controller

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.exception.DtoBindException
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import net.riversoft.worktimetransference.model.util.EmployeeComparator
import net.riversoft.worktimetransference.service.employee.EmployeeService
import net.riversoft.worktimetransference.service.admin.AdminEventService
import net.riversoft.worktimetransference.service.admin.filter.EventSearchCriteria
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.servlet.ModelAndView

import javax.servlet.http.HttpServletRequest
import javax.validation.Valid
import java.security.Principal

@Slf4j(value = "logger")
@Controller
@Profile(["dev", "prod"])
class AdminPanelController {

    private final AdminEventService eventService
    private final EmployeeService employeeService

    @Autowired
    AdminPanelController(AdminEventService eventService, EmployeeService employeeService) {
        this.eventService = eventService
        this.employeeService = employeeService
    }

    @RequestMapping("/admin-panel")
    String show(Principal principal, ModelMap model) {
        model.addAttribute("usernameField", EventFilterDTO.USERNAME_VIEW_NAME)
        model.addAttribute("dateFromField", EventFilterDTO.DATE_FROM_VIEW_NAME)
        model.addAttribute("anotherDateField", EventFilterDTO.ANOTHER_DATE_VIEW_NAME)
        model.addAttribute("filterDto", new EventFilterDTO())
        model.addAttribute("signedIn", principal.name)
        model.addAttribute("employeeList", employeeService.getOrderedEmployeeList(EmployeeComparator.FULL_NAME))
        model.addAttribute("filterList", eventService.availableCriteria)
        "admin"
    }

    @RequestMapping(value = "/admin-panel", method = RequestMethod.POST)
    String showEvents(@ModelAttribute("filterDto") @Valid EventFilterDTO filterDTO,
                      @RequestParam("criteria") EventSearchCriteria searchCriteria,
                      BindingResult result,
                      ModelMap model,
                      Principal principal)
    {
        List<Event> resultList = eventService.find(searchCriteria, filterDTO)
        model.addAttribute("usernameField", EventFilterDTO.USERNAME_VIEW_NAME)
        model.addAttribute("dateFromField", EventFilterDTO.DATE_FROM_VIEW_NAME)
        model.addAttribute("anotherDateField", EventFilterDTO.ANOTHER_DATE_VIEW_NAME)
        model.addAttribute("errors", result.getFieldErrors())
        model.addAttribute("searchResult", resultList)
        model.addAttribute("signedIn", principal.name)
        model.addAttribute("employeeList", employeeService.getOrderedEmployeeList(EmployeeComparator.FULL_NAME))
        model.addAttribute("filterList", eventService.availableCriteria)
        "admin"
    }

    @ExceptionHandler(DtoBindException.class)
    ModelAndView handleDtoBindingException(HttpServletRequest request, DtoBindException ex) {
        logger.debug("Request url: " + request.getRequestURL())
        logger.debug("Exception message: " + ex.message)

        ModelAndView mav = new ModelAndView()
        mav.addObject("exception", ex)
        mav.addObject("refererUrl", request.getRequestURL())

        mav.setViewName("error")
        mav
    }
}
