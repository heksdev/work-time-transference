package net.riversoft.worktimetransference.controller

import net.riversoft.worktimetransference.service.employee.EmployeeService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

import java.security.Principal

@Controller
@RequestMapping("/login")
@Profile(["dev", "prod"])
class LoginController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass())
    private final EmployeeService employeeService

    @Autowired
    LoginController(EmployeeService employeeService) {
        this.employeeService = employeeService
    }

    @GetMapping
    String show(Principal principal, ModelMap model) {
        if (principal.is(null)) {
            model.addAttribute("alreadyLogged", false)
            model.addAttribute("isAdmin", false)
        } else {
            model.addAttribute("alreadyLogged", true)
            model.addAttribute("isAdmin", employeeService.isAdmin(principal.name))
            model.addAttribute("signedIn", principal.name)
        }
        logger.info("Show login page...")
        "login"
    }
}
