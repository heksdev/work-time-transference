package net.riversoft.worktimetransference.controller

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.exception.DocumentInsertionException
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.util.EmployeeComparator
import net.riversoft.worktimetransference.service.employee.EmployeeService
import net.riversoft.worktimetransference.model.event.EventDTO
import net.riversoft.worktimetransference.service.UserEventService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.dao.DuplicateKeyException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.servlet.ModelAndView

import javax.servlet.http.HttpServletRequest
import javax.validation.Valid
import java.security.Principal

@Slf4j(value = "logger")
@Controller
@Profile(['dev', 'prod'])
class TargetController {

    private final EmployeeService employeeService
    private final UserEventService eventService

    @Autowired
    TargetController(EmployeeService employeeService, UserEventService eventService) {
        this.employeeService = employeeService
        this.eventService = eventService
    }

    @GetMapping(value = "/target")
    String show(ModelMap model, Principal principal) {
        logger.debug("Logged in employeeUserName: " + principal.name)
        logger.debug(principal.name + " is admin? - " + employeeService.isAdmin(principal.name).toString())

        initModel(model, principal)
        "target"
    }

    @PostMapping(value = "/add")
    String addEvent(@ModelAttribute("eventDto") @Valid EventDTO eventDTO,
                    BindingResult result,
                    Principal principal,
                    ModelMap model)
    {
        parseDto(result, model, eventDTO, principal)
        initModel(model, principal)
        "target"
    }

    private void initModel(ModelMap model, Principal principal) {
        model.addAttribute("authorized", employeeService.loadEmployeeByUserName(principal.name))
        model.addAttribute("employees", employeeService.getOrderedEmployeeList(EmployeeComparator.FULL_NAME))
        model.addAttribute("isAdmin", employeeService.isAdmin(principal.name))
        model.addAttribute("eventDto", new EventDTO())
    }

    private void parseDto(BindingResult bindingResult, ModelMap model, EventDTO dto, Principal principal) {
        if (!bindingResult.hasErrors()) {
            insertEvent(principal, dto)
            return
        }
        model.addAttribute("errors", bindingResult.getFieldErrors())
    }

    private void insertEvent(Principal principal, EventDTO dto) {
        Event event = new Event(
                employeeService.loadEmployeeByUserName(principal.name),
                employeeService.loadEmployeeByUserName(dto.employeeUserName),
                dto.dateFrom,
                dto.dateTo,
                dto.dateWorkOff)
        try {
            eventService.addNewEvent(event)
        } catch (DuplicateKeyException e) {
            logger.debug(e.message)
            throw new DocumentInsertionException("Документ с ID ${event.id} уже существует.")
        }
    }

    @ExceptionHandler(DocumentInsertionException.class)
    ModelAndView handleDocumentInsertionException(HttpServletRequest request, DocumentInsertionException ex) {
        ModelAndView mav = new ModelAndView('error')
        mav.addObject('exception', ex)
        mav.addObject('refererUrl', request.getRequestURL())
        logger.debug(ex.message)
        mav
    }
}
