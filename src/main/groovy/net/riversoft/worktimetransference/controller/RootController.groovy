package net.riversoft.worktimetransference.controller

import net.riversoft.worktimetransference.service.employee.EmployeeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

import java.security.Principal

@Controller
@RequestMapping(value = ["/", "/index", "/home"])
@Profile(["dev", "prod"])
class RootController {

    private final EmployeeService employeeService

    @Autowired
    RootController(EmployeeService employeeService) {
        this.employeeService = employeeService
    }

    @GetMapping
    String redirect(Principal principal) {
        String requestMapping
        if (principal == null) requestMapping = "/login"
        else if (employeeService.isAdmin(principal.name)) requestMapping = "/admin-panel"
        else requestMapping = "/target"
        "redirect:" + requestMapping
    }
}
