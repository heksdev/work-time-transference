package net.riversoft.worktimetransference.repository

import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

import java.time.LocalDate
import java.time.LocalDateTime

@Repository
interface EventRepository extends MongoRepository<Event, String> {
    List<Event> findByRecipientUsername(String username)
    List<Event> findByDateFromBetween(LocalDateTime dateFrom, LocalDateTime anotherDateFrom)
    List<Event> findByDateWorkOffBetween(LocalDate dateWorkOff, LocalDate anotherDateWorkOff)
    List<Event> findByRecipientUsernameAndDateFromBetween(String username, LocalDateTime dateFrom, LocalDateTime anotherDateFrom)
    List<Event> findByRecipientUsernameAndDateWorkOffBetween(String username, LocalDate dateWorkOff, LocalDate anotherDate)
}