package net.riversoft.worktimetransference.config

import com.atlassian.crowd.integration.http.HttpAuthenticator
import com.atlassian.crowd.integration.http.HttpAuthenticatorImpl
import com.atlassian.crowd.integration.springsecurity.RemoteCrowdAuthenticationProvider
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsServiceImpl
import com.atlassian.crowd.service.AuthenticationManager
import com.atlassian.crowd.service.GroupManager
import com.atlassian.crowd.service.UserManager
import com.atlassian.crowd.service.cache.BasicCache
import com.atlassian.crowd.service.cache.CacheImpl
import com.atlassian.crowd.service.cache.CachingGroupManager
import com.atlassian.crowd.service.cache.CachingGroupMembershipManager
import com.atlassian.crowd.service.cache.CachingUserManager
import com.atlassian.crowd.service.cache.SimpleAuthenticationManager
import com.atlassian.crowd.service.soap.client.SecurityServerClient
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ClassPathResource
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
@Profile(["dev", "prod"])
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final BasicCache cache = new CacheImpl(new ClassPathResource("static/crowd/crowd-ehcache.xml").getURL())

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
        http
            .authorizeRequests()
                .antMatchers("/admin-panel/**").hasAnyRole("wtt-admins")
                .antMatchers("/css/**", "/error", "/js/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().accessDeniedPage("/access-denied")
                .and()
            .formLogin()
                .loginPage("/login")
                .successHandler(new LoginAuthenticationHandler())
                .failureHandler(new ErrorsHandler())
                .usernameParameter("username")
                .passwordParameter("password")
                .permitAll()
                .and()
            .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
                .invalidateHttpSession(true)
                .permitAll()
    }

    static Properties getProps() throws IOException {
        ClassPathResource resource = new ClassPathResource("static/crowd/crowd.properties")
        Properties properties = new Properties()
        properties.load(resource.getInputStream())
        properties
    }

    @Bean
    SecurityServerClient securityServerClient() throws IOException {
        new SecurityServerClientImpl(SoapClientPropertiesImpl
                .newInstanceFromProperties(getProps()))
    }

    @Bean
    AuthenticationManager crowdAuthenticationManager() throws IOException {
        new SimpleAuthenticationManager(securityServerClient())
    }

    @Bean
    HttpAuthenticator httpAuthenticator() throws IOException {
        new HttpAuthenticatorImpl(crowdAuthenticationManager())
    }

    @Bean
    UserManager userManager() throws IOException {
        new CachingUserManager(securityServerClient(), cache)
    }

    @Bean
    GroupManager groupManager() throws IOException {
        new CachingGroupManager(securityServerClient(), cache)
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(crowdAuthenticationProvider())
    }

    @Bean
    CrowdUserDetailsService crowdUserDetailsService() throws IOException{
        CrowdUserDetailsServiceImpl cusd = new CrowdUserDetailsServiceImpl()
        cusd.setUserManager(userManager())
        cusd.setGroupMembershipManager(new CachingGroupMembershipManager(securityServerClient(), userManager(), groupManager(), cache))
        cusd.setAuthorityPrefix("ROLE_")
        cusd
    }

    @Bean
    RemoteCrowdAuthenticationProvider crowdAuthenticationProvider() throws IOException {
        return new RemoteCrowdAuthenticationProvider(crowdAuthenticationManager(), httpAuthenticator(), crowdUserDetailsService())
    }

    @Autowired
    void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(crowdUserDetailsService())
    }
}
