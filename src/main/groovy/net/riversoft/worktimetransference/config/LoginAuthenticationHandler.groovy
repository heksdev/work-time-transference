package net.riversoft.worktimetransference.config

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.model.employee.Role
import org.springframework.stereotype.Component

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.web.DefaultRedirectStrategy
import org.springframework.security.web.RedirectStrategy
import org.springframework.security.web.authentication.AuthenticationSuccessHandler

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Slf4j("logger")
@Component
class LoginAuthenticationHandler implements AuthenticationSuccessHandler {
    private static final String USER_URL_REDIRECT = "/target"
    private static final String ADMIN_URL_REDIRECT = "/admin-panel"

    private final RedirectStrategy strategy = new DefaultRedirectStrategy()

    @Override
    void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        logger.debug(">>>Logged user: " + authentication.name)
        String targetUrl = determineTargetUrl(authentication)
        strategy.sendRedirect(request, response, targetUrl)
    }

    private String determineTargetUrl(Authentication authentication) {
        List<GrantedAuthority> authorities = authentication.authorities.asList()
        boolean haveNoAuthorities = authorities.size() == 0
        if (haveNoAuthorities)
            throw new IllegalStateException("Нет прав доступа!")
        for (GrantedAuthority authority : authorities) {
            if (authority.authority.equals(Role.ROLE_ADMIN.role)) {
                return ADMIN_URL_REDIRECT
            }
        }
        USER_URL_REDIRECT
    }

    private boolean isAdminRole(String role) {
        return role.equals(Role.ROLE_ADMIN.name())
    }
}
