package net.riversoft.worktimetransference.service.employee

import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService
import com.atlassian.crowd.service.UserManager
import com.atlassian.crowd.service.soap.client.SecurityServerClient
import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.model.employee.Employee
import net.riversoft.worktimetransference.model.employee.Role
import net.riversoft.worktimetransference.model.util.EmployeeComparator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.security.core.GrantedAuthority
import org.springframework.stereotype.Service

@Slf4j("logger")
@Service
@Profile(["dev", "prod"])
class EmployeeService {

    private final SecurityServerClient securityServerClient
    private final CrowdUserDetailsService userDetailsService
    private Map<String, Employee> employeeMap
    private final EmployeeBucket employeeBucket

    @Autowired
    EmployeeService(SecurityServerClient securityServerClient, CrowdUserDetailsService userDetailsService) {
        this.securityServerClient = securityServerClient
        this.userDetailsService = userDetailsService
        loadEmployees()
        employeeBucket = new EmployeeBucket(employeeMap.values())

    }

    Employee loadEmployeeByUserName(String username) {
        employeeMap.get(username)
    }

    boolean isAdmin(String username) {
        for (GrantedAuthority authority : userDetailsService.loadUserByUsername(username).authorities) {
            logger.debug(authority.authority)
            if (authority.authority.equals(Role.ROLE_ADMIN.role)) {
                return true
            }
        }
        return false
    }

    List<Employee> getOrderedEmployeeList(EmployeeComparator comparator) {
        employeeBucket.getList(comparator)
    }

    void update() {
        loadEmployees()
        employeeBucket.modify(employeeMap.values())
    }

    private void loadEmployees() {
        logger.debug(securityServerClient.findAllGroupNames().toString())
        Map<String, Employee> employeeMap = new HashMap<>()
        securityServerClient.findAllPrincipalNames().each {
            username ->
                CrowdUserDetails userDetails = userDetailsService.loadUserByUsername(username)
                employeeMap.put(username, new Employee(userDetails.username, userDetails.fullName))
        }
        this.employeeMap = employeeMap
    }
}
