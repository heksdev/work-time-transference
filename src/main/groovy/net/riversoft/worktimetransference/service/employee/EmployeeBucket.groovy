package net.riversoft.worktimetransference.service.employee

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.model.employee.Employee
import net.riversoft.worktimetransference.model.util.EmployeeComparator

import javax.validation.constraints.NotNull

@Slf4j('logger')
class EmployeeBucket {

    private final Map<EmployeeComparator, List<Employee>> employeeListMap
    private Collection<Employee> employees

    @NotNull
    protected EmployeeBucket(Collection<Employee> employees) {
        logger.debug("employees list size - " + employees.size().toString())
        employeeListMap = new HashMap<>()
        this.employees = employees
    }

    @NotNull
    protected void modify(Collection<Employee> employees) {
        employeeListMap.each {
            entry ->
                synchronized (entry.value) {
                    entry.value.retainAll(employees)
                    entry.value.sort(entry.key.comparator)
            }
        }
        this.employees = employees
    }

    @NotNull
    protected List<Employee> getList(EmployeeComparator comparator) {
        if (employeeListMap.containsKey(comparator)) {
            return employeeListMap.get(comparator)
        }
        return sortAndMap(comparator)
    }

    private List<Employee> sortAndMap(EmployeeComparator comparator) {
        List<Employee> comparedList = employees.toSorted(comparator.comparator)
        synchronized (employeeListMap) {
            employeeListMap.put(comparator, comparedList)
        }
        comparedList
    }
}
