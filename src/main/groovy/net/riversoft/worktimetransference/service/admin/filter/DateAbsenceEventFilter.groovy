package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.exception.DtoBindException
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import net.riversoft.worktimetransference.repository.EventRepository

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Slf4j("logger")
class DateAbsenceEventFilter extends EventFilter {

    static final EventSearchCriteria CRITERIA = EventSearchCriteria.DATE_ABSENCE

    DateAbsenceEventFilter(EventRepository repository) {
        super(repository)
    }

    @Override
    List<Event> find(EventFilterDTO filterDTO) {
        logger.debug("Фильтрация по " + CRITERIA.name() + ": " + CRITERIA.description)
        logger.debug("Параметры фильтрации: " + filterDTO.toString())

        validateDto(filterDTO)
        repository.findByDateFromBetween(LocalDateTime.of(filterDTO.dateFrom, LocalTime.of(0, 0)),
                LocalDateTime.of(filterDTO.anotherDate, LocalTime.of(0, 0)))
    }

    private void validateDto(EventFilterDTO dto) {
        if (dto.dateFrom == null)
            throw new DtoBindException(CRITERIA, EventFilterDTO.DATE_FROM_VIEW_NAME)
        if (dto.anotherDate == null)
            dto.setAnotherDate(LocalDate.now())
    }
}
