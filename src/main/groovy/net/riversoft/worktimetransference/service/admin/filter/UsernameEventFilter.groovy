package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.exception.DtoBindException
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import net.riversoft.worktimetransference.repository.EventRepository

@Slf4j("logger")
class UsernameEventFilter extends EventFilter {

    static final EventSearchCriteria CRITERIA = EventSearchCriteria.USERNAME

    UsernameEventFilter(EventRepository repository) {
        super(repository)
    }

    @Override
    List<Event> find(EventFilterDTO filterDTO) {
        logger.debug("Фильтрация по " + CRITERIA.name() + ": " + CRITERIA.description)
        logger.debug("Параметры фильтрации: " + filterDTO.toString())

        validateDto(filterDTO)
        repository.findByRecipientUsername(filterDTO.employeeUsername)
    }

    private void validateDto(EventFilterDTO dto) {
        if (dto.employeeUsername == null) throw new DtoBindException(CRITERIA, EventFilterDTO.USERNAME_VIEW_NAME)
    }
}
