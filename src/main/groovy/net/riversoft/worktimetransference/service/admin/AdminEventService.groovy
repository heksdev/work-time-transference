package net.riversoft.worktimetransference.service.admin

import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import net.riversoft.worktimetransference.repository.EventRepository
import net.riversoft.worktimetransference.service.admin.filter.DateAbsenceEventFilter
import net.riversoft.worktimetransference.service.admin.filter.DateWorkOffEventFilter
import net.riversoft.worktimetransference.service.admin.filter.DefaultEventFilter
import net.riversoft.worktimetransference.service.admin.filter.EventSearchCriteria
import net.riversoft.worktimetransference.service.admin.filter.EventFilter
import net.riversoft.worktimetransference.service.admin.filter.UsernameDateAbsenceEventFilter
import net.riversoft.worktimetransference.service.admin.filter.UsernameDateWorkOffEventFilter
import net.riversoft.worktimetransference.service.admin.filter.UsernameEventFilter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AdminEventService {

    private final EventRepository repository
    private final Map<EventSearchCriteria, EventFilter> filterMap = new HashMap<>()

    @Autowired
    AdminEventService(EventRepository repository) {
        this.repository = repository
        initFilterMap()
    }

    private void initFilterMap() {
        filterMap.put(DefaultEventFilter.CRITERIA, new DefaultEventFilter(repository))
        filterMap.put(UsernameEventFilter.CRITERIA, new UsernameEventFilter(repository))
        filterMap.put(DateAbsenceEventFilter.CRITERIA, new DateAbsenceEventFilter(repository))
        filterMap.put(DateWorkOffEventFilter.CRITERIA, new DateWorkOffEventFilter(repository))
        filterMap.put(UsernameDateAbsenceEventFilter.CRITERIA, new UsernameDateAbsenceEventFilter(repository))
        filterMap.put(UsernameDateWorkOffEventFilter.CRITERIA, new UsernameDateWorkOffEventFilter(repository))
    }

    List<Event> find(EventSearchCriteria searchCriteria, EventFilterDTO filterDTO) {
        filterMap.get(searchCriteria).find(filterDTO)
    }

    Set<EventSearchCriteria> getAvailableCriteria() {
        filterMap.keySet().toSorted({c1, c2 -> c1.sequence <=> c2.sequence})
    }
}
