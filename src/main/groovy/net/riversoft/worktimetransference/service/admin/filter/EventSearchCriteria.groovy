package net.riversoft.worktimetransference.service.admin.filter

enum EventSearchCriteria {
    DEFAULT("Все записи", 1),
    USERNAME("По сотруднику", 2),
    DATE_ABSENCE("По дате отсутствия", 3),
    USERNAME_DATE_ABSENCE("По сотруднику и дате отсутствия", 5),
    DATE_WORK_OFF("По дате отработки", 4),
    USERNAME_DATE_WORK_OFF("По сотруднику и дате отработки", 6)

    private final String description
    private final int sequence

    EventSearchCriteria(String description, int sequence) {
        this.description = description
        this.sequence = sequence
    }

    String getDescription() {
        description
    }

    int getSequence() {
        sequence
    }
}
