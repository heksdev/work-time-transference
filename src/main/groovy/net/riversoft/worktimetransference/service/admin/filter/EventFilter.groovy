package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import net.riversoft.worktimetransference.repository.EventRepository

@Slf4j("logger")
abstract class EventFilter {

    protected final EventRepository repository

    EventFilter(EventRepository repository) {
        this.repository = repository
    }

    abstract List<Event> find(EventFilterDTO filterDTO)
}