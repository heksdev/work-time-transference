package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.exception.DtoBindException
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import net.riversoft.worktimetransference.repository.EventRepository

import java.time.LocalDate

@Slf4j("logger")
class UsernameDateWorkOffEventFilter extends EventFilter {

    static final EventSearchCriteria CRITERIA = EventSearchCriteria.USERNAME_DATE_WORK_OFF

    UsernameDateWorkOffEventFilter(EventRepository repository) {
        super(repository)
    }

    @Override
    List<Event> find(EventFilterDTO filterDTO) {
        logger.debug("Фильтрация по " + CRITERIA.name() + ": " + CRITERIA.description)
        logger.debug("Параметры фильтрации: " + filterDTO.toString())

        validateDto(filterDTO)
        repository.findByRecipientUsernameAndDateWorkOffBetween(
                filterDTO.employeeUsername,
                filterDTO.dateFrom,
                filterDTO.anotherDate)
    }

    private validateDto(EventFilterDTO dto) {
        if (dto.employeeUsername == null || dto.dateFrom == null)
            throw new DtoBindException(CRITERIA, EventFilterDTO.USERNAME_VIEW_NAME, EventFilterDTO.DATE_FROM_VIEW_NAME)
        if (dto.anotherDate == null)
            dto.setAnotherDate(LocalDate.now())
    }
}
