package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.exception.DtoBindException
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import net.riversoft.worktimetransference.repository.EventRepository

import java.time.LocalDate

@Slf4j("logger")
class DateWorkOffEventFilter extends EventFilter {

    static final EventSearchCriteria CRITERIA = EventSearchCriteria.DATE_WORK_OFF

    DateWorkOffEventFilter(EventRepository repository) {
        super(repository)
    }

    @Override
    List<Event> find(EventFilterDTO filterDTO) {
        logger.debug("Фильтрация по " + CRITERIA.name() + ": " + CRITERIA.description)
        logger.debug("Параметры фильтрации: " + filterDTO.toString())

        validateDto(filterDTO)
        repository.findByDateWorkOffBetween(filterDTO.dateFrom, filterDTO.anotherDate)
    }

    private void validateDto(EventFilterDTO dto) {
        if (dto.dateFrom == null) throw new DtoBindException(CRITERIA, EventFilterDTO.DATE_FROM_VIEW_NAME)
        if (dto.anotherDate == null)
            dto.setAnotherDate(LocalDate.now())
    }
}
