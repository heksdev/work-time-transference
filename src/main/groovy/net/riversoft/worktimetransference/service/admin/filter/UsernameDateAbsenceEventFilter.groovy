package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.exception.DtoBindException
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import net.riversoft.worktimetransference.repository.EventRepository

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Slf4j("logger")
class UsernameDateAbsenceEventFilter extends EventFilter {

    static final EventSearchCriteria CRITERIA = EventSearchCriteria.USERNAME_DATE_ABSENCE

    UsernameDateAbsenceEventFilter(EventRepository repository) {
        super(repository)
    }

    @Override
    List<Event> find(EventFilterDTO filterDTO) {
        logger.debug("Фильтрация по " + CRITERIA.name() + ": " + CRITERIA.description)
        logger.debug("Параметры фильтрации: " + filterDTO.toString())

        validateDto(filterDTO)
        repository.findByRecipientUsernameAndDateFromBetween(filterDTO.employeeUsername,
                LocalDateTime.of(filterDTO.dateFrom, LocalTime.of(0, 0)),
                LocalDateTime.of(filterDTO.anotherDate, LocalTime.of(0, 0)))
    }

    private void validateDto(EventFilterDTO dto) {
        if (dto.employeeUsername == null || dto.dateFrom == null)
            throw new DtoBindException(CRITERIA, EventFilterDTO.USERNAME_VIEW_NAME, EventFilterDTO.DATE_FROM_VIEW_NAME)
        if (dto.anotherDate == null) {
            dto.setAnotherDate(LocalDate.now())
        }
    }
}
