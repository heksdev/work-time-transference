package net.riversoft.worktimetransference.service.admin.filter

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.model.event.EventFilterDTO
import net.riversoft.worktimetransference.repository.EventRepository

@Slf4j("logger")
class DefaultEventFilter extends EventFilter {

    static final EventSearchCriteria CRITERIA = EventSearchCriteria.DEFAULT

    DefaultEventFilter(EventRepository repository) {
        super(repository)
    }

    @Override
    List<Event> find(EventFilterDTO filterDTO) {
        logger.debug("Фильтрация по " + CRITERIA.name() + ": " + CRITERIA.description)
        logger.debug("Параметры фильтрации: " + filterDTO.toString())

        repository.findAll()
    }
}
