package net.riversoft.worktimetransference.service

import net.riversoft.worktimetransference.model.event.Event
import net.riversoft.worktimetransference.repository.EventRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserEventService {

    @Autowired
    private final EventRepository repository

    UserEventService(EventRepository repository) {
        this.repository = repository
    }

    void addNewEvent(Event event) {
        synchronized (repository) {
            repository.insert(event)
            repository.notifyAll()
        }
    }
}
