package net.riversoft.worktimetransference.model.employee

class Employee {

    private final String username
    private final String fullName

    Employee(String username, String fullName) {
        this.username = username
        this.fullName = fullName
    }

    String getUsername() {
        username
    }

    String getFullName() {
        fullName
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Employee employee = (Employee) o

        if (username != employee.username) return false
        if (fullName != employee.fullName) return false

        true
    }

    int hashCode() {
        int result
        result = (fullName != null ? fullName.hashCode() : 0)
        result = 31 * result + (username != null ? username.hashCode() : 0)
        result
    }


    @Override
    String toString() {
        "Employee: " + "\n" +
                "\tFull name: " + fullName + "\n" +
                "\tUsername: " + username + "\n"
    }
}
