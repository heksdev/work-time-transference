package net.riversoft.worktimetransference.model.employee

enum Role {
    ROLE_USER("ROLE_jira-users"),
    ROLE_ADMIN("ROLE_wtt-admins")

    private final String role

    Role(String role) {
        this.role = role
    }

    String getRole() {
        role
    }
}