package net.riversoft.worktimetransference.model.event

import org.springframework.format.annotation.DateTimeFormat

import java.time.LocalDate

class EventFilterDTO {
    static final USERNAME_VIEW_NAME = "Сотрудник"
    static final DATE_FROM_VIEW_NAME = "С даты"
    static final ANOTHER_DATE_VIEW_NAME = "По дату"

    private String employeeUsername
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dateFrom
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate anotherDate

    String getEmployeeUsername() {
        return employeeUsername
    }

    void setEmployeeUsername(String employeeUsername) {
        this.employeeUsername = employeeUsername
    }

    LocalDate getDateFrom() {
        return dateFrom
    }

    void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom
    }

    LocalDate getAnotherDate() {
        return anotherDate
    }

    void setAnotherDate(LocalDate anotherDate) {
        this.anotherDate = anotherDate
    }

    @Override
    String toString() {
        return "EventFilterDTO: username - " + employeeUsername +
                "; date from - " + dateFrom +
                "; anotherDate - " + anotherDate
    }
}
