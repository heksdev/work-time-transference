package net.riversoft.worktimetransference.model.event

import org.hibernate.validator.constraints.NotBlank
import org.springframework.format.annotation.DateTimeFormat

import javax.validation.constraints.NotNull
import java.time.LocalDate
import java.time.LocalDateTime

class EventDTO {

    @NotNull @NotBlank
    private String employeeUserName
    @NotNull @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateFrom
    @NotNull @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime dateTo
    @NotNull @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate dateWorkOff

    String getEmployeeUserName() {
        return employeeUserName
    }

    LocalDateTime getDateFrom() {
        return dateFrom
    }

    LocalDateTime getDateTo() {
        return dateTo
    }

    LocalDate getDateWorkOff() {
        return dateWorkOff
    }

    void setEmployeeUserName(String employee) {
        this.employeeUserName = employee
    }

    void setDateFrom(LocalDateTime dateFrom) {
        this.dateFrom = dateFrom
    }

    void setDateTo(LocalDateTime dateTo) {
        this.dateTo = dateTo
    }

    void setDateWorkOff(LocalDate dateWorkOff) {
        this.dateWorkOff = dateWorkOff
    }

    @Override
    String toString() {
        "FormEvent: " + "\n" +
                "\t" + employeeUserName + "\n" +
                "\t" + dateFrom.toString() + "\n" +
                "\t" + dateTo.toString() + "\n" +
                "\t" + dateWorkOff.toString() + "\n"
    }
}
