package net.riversoft.worktimetransference.model.event

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.model.employee.Employee
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId

@Slf4j('logger')
@Document(collection = 'event')
class Event {

    @Id
    private final String id
    private final Employee initiator
    private final Employee recipient
    private final LocalDateTime dateFrom
    private final LocalDateTime dateTo
    private final LocalDate dateWorkOff

    Event(Employee initiator, Employee recipient, LocalDateTime dateFrom, LocalDateTime dateTo, LocalDate dateWorkOff) {
        this.initiator = initiator
        this.recipient = recipient
        this.dateFrom = dateFrom
        this.dateTo = dateTo
        this.dateWorkOff = dateWorkOff
        this.id = generateId(initiator, recipient, dateFrom)
        logger.debug("created new Event record with ${id}")
    }

    private String generateId(Employee initiator, Employee recipient, LocalDateTime dateFrom) {
        "${initiator.username}_${recipient.username}_${dateFrom.atZone(ZoneId.systemDefault()).toEpochSecond()}"
    }

    String getId() {
        return id
    }

    Employee getInitiator() {
        return initiator
    }

    Employee getRecipient() {
        return recipient
    }

    LocalDateTime getDateFrom() {
        return dateFrom
    }

    LocalDateTime getDateTo() {
        return dateTo
    }

    LocalDate getDateWorkOff() {
        return dateWorkOff
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Event that = (Event) o

        if (dateFrom != that.dateFrom) return false
        if (dateTo != that.dateTo) return false
        if (dateWorkOff != that.dateWorkOff) return false
        if (id != that.id) return false
        if (initiator != that.initiator) return false
        if (recipient != that.recipient) return false

        return true
    }

    int hashCode() {
        int result
        result = (id != null ? id.hashCode() : 0)
        result = 31 * result + (initiator != null ? initiator.hashCode() : 0)
        result = 31 * result + (recipient != null ? recipient.hashCode() : 0)
        result = 31 * result + (dateFrom != null ? dateFrom.hashCode() : 0)
        result = 31 * result + (dateTo != null ? dateTo.hashCode() : 0)
        result = 31 * result + (dateWorkOff != null ? dateWorkOff.hashCode() : 0)
        return result
    }

    @Override
    String toString() {
        "Event: " + "\n" +
                "\t" + initiator.toString() + "\n" +
                "\t" + recipient.toString() + "\n" +
                "\t" + dateFrom.toString() + "\n" +
                "\t" + dateTo.toString() + "\n" +
                "\t" + dateWorkOff.toString() + "\n"
    }
}
