package net.riversoft.worktimetransference.model.util

import net.riversoft.worktimetransference.model.employee.Employee

enum EmployeeComparator {
    FULL_NAME(new Comparator<Employee>() {
        @Override
        int compare(Employee o1, Employee o2) {
            o1.fullName <=> o2.fullName
        }
    }),
    USER_NAME(new Comparator<Employee>() {
        @Override
        int compare(Employee o1, Employee o2) {
            o1.username <=> o2.username
        }
    })

    private final Comparator<Employee> comparator

    EmployeeComparator(Comparator<Employee> comparator) {
        this.comparator = comparator
    }

    Comparator<Employee> getComparator() {
        comparator
    }
}