package net.riversoft.worktimetransference.model.util

import groovy.util.logging.Slf4j
import net.riversoft.worktimetransference.service.employee.EmployeeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Profile(['dev', 'prod'])
@Component
@EnableScheduling
@Slf4j('logger')
class LoadUsersTask {

    private final EmployeeService employeeService

    @Autowired
    LoadUsersTask(EmployeeService employeeService) {
        this.employeeService = employeeService
    }

    @Scheduled(cron = "0 0 7,19 * * MON-FRI")
    void syncUsers() {
        logger.debug(">>> Start synchronization user-list...")
        employeeService.update()
    }
}
