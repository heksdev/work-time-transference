<html>
<head>
    <title>Admin panel</title>
    <meta charset="UTF-8" content="text/html">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<#--NAVIGATION-->
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Riversoft</a>
        </div>
        <ul class="nav navbar-nav">
            <li><a href="/target">Фиксация опозданий</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><p class="navbar-text">Администратор  <strong>${signedIn}</strong></p></li>
            <li><a href="/logout">Выйти</a></li>
        </ul>
    </div>
</nav>
<#-- END NAV-->
<#--CONTENT-->
<div class="container">
    <div class="row">
        <form class="form-horizontal" action="/admin-panel" method="post">
            <div class="form-group">
                <label class="control-label col-sm-4" for="search-criteria">Фильтр</label>
                <div class="col-sm-6">
                    <select class="form-control" id="search-criteria" name="criteria">
                    <#if filterList?size != 0>
                        <#list filterList as filter>
                            <option value="${filter}">${filter.description}</option>
                        </#list>
                    </#if>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="employee-list">${usernameField}</label>
                <div class="col-sm-6">
                    <select class="form-control" id="employee-list" name="employeeUsername">
                    <#if employeeList?size != 0>
                        <#list employeeList as employee>
                            <option value="${employee.username}">${employee.fullName + " \"" + employee.username + "\""}</option>
                        </#list>
                    </#if>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="date-from">${dateFromField}</label>
                <div class="col-sm-6">
                    <input type="date" class="form-control" id="date-from" name="dateFrom">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4" for="another-date">${anotherDateField}</label>
                <div class="col-sm-6">
                    <input type="date" class="form-control" id="another-date" name="anotherDate">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="submit" class="btn btn-default">Найти</button>
                </div>
            </div>
        </form>
    </div>
</div>
<#--TABLE-->
<#if searchResult??>
<div class="container">
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Кто отмечал</th>
                <th>Кого отмечали</th>
                <th>Отсутствовал с</th>
                <th>Отсутствовал по</th>
                <th>Дата отработки</th>
            </tr>
            </thead>
            <tbody>
                <#if searchResult?size == 0>
                <tr>
                    <td>Пусто</td>
                </tr>
                <#else>
                    <#list searchResult as event>
                    <tr>
                        <td>${event.initiator.username + "\n" + event.initiator.fullName}</td>
                        <td>${event.recipient.username + "\n" + event.recipient.fullName}</td>
                        <td>${event.dateFrom}</td>
                        <td>${event.dateTo}</td>
                        <td>${event.dateWorkOff}</td>
                    </tr>
                    </#list>
                </#if>
            </tbody>
        </table>
    </div>
</div>
</#if>
<#if errors??>
<table class="table">
    <#list errors as error>
        <tr>
            <td>Ошибка заполнения поля - ${error.field}</td>
        </tr>
    </#list>
</table>
</#if>
</body>
</html>