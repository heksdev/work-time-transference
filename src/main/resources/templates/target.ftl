<!DOCTYPE html>
<html>
<head>
    <meta content="text/html" charset="UTF-8"/>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<#--top nav-->
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Riversoft</a>
        </div>
        <ul class="nav navbar-nav">
        <#if isAdmin>
            <li><a href="/admin-panel">Админ панель</a></li>
        </#if>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><p class="navbar-text">Вы вошли как <strong>${authorized.username}</strong></p></li>
            <li><a href="/logout">Выйти</a></li>
        </ul>
    </div>
</nav>
<#--form-->
<div class="row">
    <div class="center-block col-md-4 col-sm-4 col-lg-4" style="float: none; margin-bottom: 50px">
        <p class="h3 text-center">Фиксация опозданий сотрудников</p>
    </div>
</div>
<div class="container">
    <div class="center-block col-md-6" style="float: none">
        <form class="form-horizontal" action="/add" method="post">
            <div class="form-group">
                <label class="control-label col-sm-3" for="employee-list">Сотрудник</label>
                <div class="col-sm-9">
                    <select class="form-control" id="employee-list" name="employeeUserName">
                        <option value="${authorized.username}">${authorized.fullName + " \"" + authorized.username + "\""}</option>
                        <#list employees as employee>
                            <option value="${employee.username}">${employee.fullName + " \"" + employee.username + "\""}</option>
                        </#list>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="date-from">Отсутствует с</label>
                <div class="col-sm-9">
                    <input type="datetime-local" class="form-control" id="date-from" name="dateFrom">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="date-to">Отсутствует по</label>
                <div class="col-sm-9">
                    <input type="datetime-local" class="form-control" id="date-to" name="dateTo">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="date-work-off">Дата отработки</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control" id="date-work-off" name="dateWorkOff">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-default">Отправить</button>
                </div>
            </div>
        </form>
    </div>
</div>
<#if errors??>
<table class="table">
    <#list errors as error>
        <tr>
            <td>Ошибка заполнения поля - ${error.field}</td>
        </tr>
    </#list>
</table>
</#if>
</body>
</html>