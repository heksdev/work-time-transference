<html>
<head>
    <title>Login</title>
    <meta charset="UTF-8" content="text/html"/>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Riversoft</a>
        </div>
        <ul class="nav navbar-nav">
            <#if alreadyLogged>
                <li><a href="/target">Фиксация опозданий</a></li>
                <#if isAdmin>
                    <li><a href="/admin-panel">Админ панель</a></li>
                </#if>
            </#if>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <#if alreadyLogged>
                <li><p class="navbar-text">Администратор  <strong>${signedIn}</strong></p></li>
                <li><a href="/logout">Выйти</a></li>
            </#if>
        </ul>
    </div>
</nav>
<div class="center-block col-md-4" style="float: none">
    <h1 class="center-block col-md-4" style="float: none">Войти</h1>
    <form class="form-inline" action="/login" method="post">
        <div class="form-group">
            <label for="username">Логин</label>
            <input type="text" class="form-control" name="username" id="username"/>
        </div>
        <div class="form-group">
            <label for="password">Пароль</label>
            <input type="password" class="form-control" name="password" id="password"/>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
</body>
</html>